export default {
  actionIcon: 'StarIcon',
  highlightColor: 'warning',
  data: [
    {index: 0, label: 'Home',   url: '/',      labelIcon: 'HomeIcon',     highlightAction: false},
    {index: 1, label: 'Prepaids Cards', url: '/prepaid', labelIcon: 'FileIcon', highlightAction: false},
  ]
}
