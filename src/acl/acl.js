import Vue from "vue"
import { AclInstaller, AclCreate, AclRule } from "vue-acl"
import router from "@/router"

Vue.use(AclInstaller)



let userInfo = JSON.parse(localStorage.getItem("smppUserInfo"))
let initialRole = userInfo && userInfo.userRole ?  userInfo.userRole: 'public'

export default new AclCreate({
  initial: initialRole,
  notfound: "/pages/login",
  router,
  acceptLocalRules: true,
  globalRules: {
    super: new AclRule("ROLE_SUPERADMIN").generate(),
    admin: new AclRule("ROLE_ADMIN").generate(),
    client: new AclRule("ROLE_CLIENT").or("ROLE_ADMIN").or('ROLE_SUPERADMIN').generate(),
    public: new AclRule("public").or("ROLE_CLIENT").or("ROLE_ADMIN").or("ROLE_SUPERADMIN").generate(),
  }
})
