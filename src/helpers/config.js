export default {
  get apiUrl () {
    const getUrl = window.location
    const domain =  process.env.NODE_ENV === 'production'
        ? getUrl.protocol + '//api.cactusprepaid.com/v1'
        : 'http://cactusprepaidapi.cu/v1'
    return domain
  }
}
