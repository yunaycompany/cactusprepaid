/**
 * Created by yosbel on 17/12/18.
 */

import api from '../helpers/auth'

export const userService = {
    login,
    register,
    confirm,
    refreshToken,
    logout,
    getUserCards,
    getCardsRequest,
    getCountries,
    getStates,
    getClients,
    requestCard,
    contact

};

function login(user) {
    return api.post(`/login`, user);

}
function register(user) {
    return api.post(`/signup`, user);

}
function requestCard(payload) {
    return api.post(`/card`, payload);

}
function contact(payload) {
    return api.post(`/contact`, payload);

}
function confirm(token) {
    return api.post(`/confirm/`+token, {});

}
function refreshToken() {
    return api.post("/refresh", {
        accessToken: localStorage.getItem("accessToKen")
    })
}

function getUserCards() {

    return api.get(`/user/cards`);

}
function getCardsRequest() {

    return api.get(`/user/request`);

}

function getCountries() {

    return api.get(`/country`);

}
function getStates(countryId) {

    return api.get(`/state/`+countryId);

}

function getClients() {

    return api.get(`/clients`);

}





function logout() {
    localStorage.removeItem('smpp');
    localStorage.clear();
}



